from setuptools import setup

setup(
    name='alpha-public-registry-grpc',
    version='1.0.3',
    packages=[''],
    url='https://gitlab.com/alphaticks/alpha-public-registry-grpc',
    license='copyright',
    author='Alphaticks',
    description='gRPC code for alpha public registry client'
)
